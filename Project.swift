import ProjectDescription

let project = Project(
    name: "RSSReader",
    targets: [
        Target(
            name: "RSSReader",
            platform: .iOS,
            product: .app,
            bundleId: "udev.dev.RSSReader",
            infoPlist: "Targets/RSSReader/Sources/Info.plist", //.extendingDefault(with: infoPlist),
            sources: [
                "Targets/RSSReader/Sources/**"
            ],
            resources: [
                "Targets/RSSReader/Resources/**"
            ]
        ),
        Target(name: "RSSReaderTests",
               platform: .iOS,
               product: .unitTests,
               bundleId: "udev.dev.RSSReaderTests",
               infoPlist: "Targets/RSSReader/Tests/UnitTests/Info.plist",
               sources: [
                "Targets/RSSReader/Tests/UnitTests/**",
                "Targets/RSSReader/Sources/**"
               ],
               resources: [],
               dependencies: []
        ),
        Target(name: "RSSReaderUITests",
               platform: .iOS,
               product: .uiTests,
               bundleId: "udev.dev.RSSReaderUITests",
               infoPlist: "Targets/RSSReader/Tests/UITests/Info.plist",
               sources: [
                "Targets/RSSReader/Tests/UITests/**",
                "Targets/RSSReader/Sources/**"
               ],
               resources: [],
               dependencies: [
                .target(name: "RSSReader"),
               ]
        )
    ]
)


import XCTest

extension XCUIElementQuery {
    subscript(id: AccessibilityIdentifier) -> XCUIElement {
        get {
            matching(identifier: id.rawValue).firstMatch
        }
    }
}

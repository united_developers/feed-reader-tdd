import XCTest

extension XCUIApplication {
    func setProcessStages(_ arguments: [AppProcessStages]) {
        launchArguments = arguments.map { $0.rawValue }
    }
}

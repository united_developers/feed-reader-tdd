import XCTest

class RSSReaderUITests: XCTestCase {
    var app: XCUIApplication!
    var subscribeFeedPage: FeedPage!
    
    override func setUpWithError() throws {
        continueAfterFailure = false
        
        app = XCUIApplication()
        app.setProcessStages([.fakeFeedParsing, .removeSubscriptions])
        app.launch()
        
        subscribeFeedPage = FeedPage(app: app)
    }
    
    func test_CanNotSubscribe_ToNonURL() throws {
        XCTAssertEqual(subscribeFeedPage.feedAddress, "")
        XCTAssertFalse(subscribeFeedPage.canSubscribe)
        
        subscribeFeedPage.typeURL("not url string")
        XCTAssertFalse(subscribeFeedPage.canSubscribe)
    }
    
    func test_CanNotSubscribe_WhenURLHasNotRSSFeed() {
        subscribeFeedPage.typeURL("https://apple.com/")
        subscribeFeedPage.subscribe()
        
        XCTAssertTrue(subscribeFeedPage.errorMessagePresent("There is no RSS feed"))
    }
    
    func test_AfterImport_ShowsSubscriptionsPage_WithNewlyAddedSubscription() {
        subscribeFeedPage.typeURL(lh.absoluteString)
        subscribeFeedPage.subscribe()
        
        subscribeFeedPage.waitUntilHidden(in: self)
        
        let subscriptionsPage = SubscriptionsPage(app: app)
        
        let expectedSubscription = SubscriptionElement(title: lhFeed.title, subtitle: lhFeed.url.absoluteString)
        
        XCTAssertEqual(subscriptionsPage.subscriptions(), [expectedSubscription])
    }
}

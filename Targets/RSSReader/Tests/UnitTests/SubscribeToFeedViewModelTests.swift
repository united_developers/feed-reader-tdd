import XCTest

class SubscribeToFeedViewModelTests: XCTestCase {
    
    var subject: FeedViewModel!
    
    private func makeSubject(extractor: FeedExtracting = DummyFeedInfoExtractor(), saver: FeedInfoSaving = DummyFeedInfoSaver()) -> FeedViewModel {
        FeedViewModel(feedInfoExtractor: extractor, feedInfoSaver: saver)
    }
    
    func test_Initially_HasNoUserInput_And_CanNotSubscribe() {
        subject = makeSubject()
        
        XCTAssertEqual(subject.userInput, "")
        XCTAssertEqual(subject.canSubscribe, false)
        XCTAssertEqual(subject.errorMessage, "")
        XCTAssertEqual(subject.errorMessageHidden, true)
        XCTAssertEqual(subject.subscribedSuccessfully, false)
    }
    
    func test_WhenUserTypesNonURL_ThenCanNotSubscribe() {
        subject = makeSubject()
        subject.typeText("not a link")
        XCTAssertEqual(subject.canSubscribe, false)
    }
    
    func test_WhenUserTypesURL_ThenCanSubscribe() {
        subject = makeSubject()
        subject.typeText("http://rss.cnn.com/rss/cnn_topstories.rss")
        XCTAssertEqual(subject.canSubscribe, true)
    }
    
    func test_WhenUserTypesURL_AndSubscribes_ThenUsesRSSFeedExtractor() throws {
        let extractor = SpyingFeedInfoExtractor()
        subject = makeSubject(extractor: extractor)
        
        subject.typeText("http://rss.cnn.com/rss/cnn_topstories.rss")
        subject.subscribe()
        
        let website = try XCTUnwrap(extractor.website)
        XCTAssertEqual(website, URL(string: "http://rss.cnn.com/rss/cnn_topstories.rss")!)
    }
    
    func test_WhenUserTypesSiteAddressWithoutRSSFeed_AndAttemptsSubscribe_ThenShowsErrorMessage() throws {
        let extractor = FailingFeedInfoExtractor(error: .empty)
        subject = makeSubject(extractor: extractor)
        
        subject.typeText("https://apple.com/")
        subject.subscribe()
        
        let message = try XCTUnwrap(subject.errorMessage)
        XCTAssertGreaterThan(message.count, 0)
        XCTAssertFalse(subject.errorMessageHidden)
    }
    
    func test_GivenRSSFeedInfoWasExtracted_ThenSavesIt_AndNotifiesDone() throws {
        let info = FeedInfo(title: "foo", url: URL(string: "https://example.org/")!)
        let extractor = SucceedingFeedInfoExtractor(feedInfo: info)
        let saver = FakeFeedInfoSaver()
        
        subject = makeSubject(extractor: extractor, saver: saver)
        
        subject.typeText("https://some-website.com/")
        subject.subscribe()
        
        let savedInfo = try XCTUnwrap(saver.savedInfo)
        XCTAssertEqual(savedInfo, info)
        XCTAssertEqual(subject.subscribedSuccessfully, true)
    }
    
    func test_GivenRSSFeedInfoWasExtracted_ButNotSaved_ThenShowsErrorMessage() throws {
        let info = FeedInfo(title: "foo", url: URL(string: "https://example.org/")!)
        let extractor = SucceedingFeedInfoExtractor(feedInfo: info)
        let saver = FailingFeedInfoSaver()
        
        subject = makeSubject(extractor: extractor, saver: saver)
        
        subject.typeText("https://some-website.com/")
        subject.subscribe()
        
        let message = try XCTUnwrap(subject.errorMessage)
        XCTAssertGreaterThan(message.count, 0)
        XCTAssertFalse(subject.errorMessageHidden)
    }
}

import Foundation

class FeedViewModel: NSObject {
    private(set) var userInput = ""
    private(set) var errorMessage = ""
    
    @objc dynamic private(set) var canSubscribe = false
    @objc dynamic private(set) var errorMessageHidden = true
    @objc dynamic private(set) var subscribedSuccessfully = false
    
    private let feedInfoExtractor: FeedExtracting
    private let feedInfoSaver: FeedInfoSaving
    
    init(feedInfoExtractor: FeedExtracting, feedInfoSaver: FeedInfoSaving) {
        self.feedInfoExtractor = feedInfoExtractor
        self.feedInfoSaver = feedInfoSaver
    }
    
    func typeText(_ text: String) {
        canSubscribe = URL(string: text) != nil
        userInput = text
    }
    
    func subscribe() {
        guard let url = URL(string: userInput) else { return }
        
        feedInfoExtractor.feeds(from: url) { [weak self] result in
            self?.handleFeedInfoExtractionResult(result)
        }
    }
    
    private func handleFeedInfoExtractionResult(_ result: Result<FeedInfo, ExtractingError>) {
        switch result {
        case .success(let feedInfo):
            handleSuccessfulExtraction(feedInfo)
        case .failure(let error):
            handleFailedExtraction(error)
        }
    }
    
    private func handleSuccessfulExtraction(_ feedInfo: FeedInfo) {
        feedInfoSaver.save(feedInfo) { [weak self] result in
            self?.handleFeedInfoSavingResult(result)
        }
    }
    
    private func handleFailedExtraction(_ error: ExtractingError) {
        switch error {
        case .empty:
            errorMessage = "There is no RSS feed"
        }
        
        errorMessageHidden = false
    }
    
    private func handleFeedInfoSavingResult(_ result: Result<Void, FeedInfoSavingError>) {
        switch result {
        case .success:
            subscribedSuccessfully = true
        case .failure:
            errorMessage = "Can't save subscription"
            errorMessageHidden = false
        }
    }
}

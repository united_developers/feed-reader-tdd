import Foundation

class FakeFeedExtractor: FeedExtracting {
    func feeds(from website: URL, completion: @escaping (Result<FeedInfo, ExtractingError>) -> Void) {
        if website.host == "daringfireball.net" {
            let stubFeed = FeedInfo(title: "Daring Fireball", url: URL(string: "https://daringfireball.net/feeds/articles")!)
            completion(.success(stubFeed))
        } else {
            completion(.failure(.empty))
        }
    }
}

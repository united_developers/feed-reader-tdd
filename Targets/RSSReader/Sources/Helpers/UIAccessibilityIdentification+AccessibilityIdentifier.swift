import UIKit

extension UIAccessibilityIdentification {
    var accessibilityId: AccessibilityIdentifier? {
        get {
            if let id = accessibilityIdentifier {
                return AccessibilityIdentifier.init(rawValue: id)
            }
            return nil
        }
        set {
            accessibilityIdentifier = newValue?.rawValue
        }
    }
}

import Foundation

extension ProcessInfo {
    var appArguments: [AppProcessStages] {
        arguments.compactMap { AppProcessStages(rawValue: $0) }
    }
}

import Foundation

class DummyFeedInfoExtractor: FeedExtracting {
    func feeds(from website: URL,completion: @escaping (Result<FeedInfo, ExtractingError>) -> Void) {}
}

class SucceedingFeedInfoExtractor: FeedExtracting {
    private let feedInfo: FeedInfo
    
    init(feedInfo: FeedInfo) {
        self.feedInfo = feedInfo
    }
    
    func feeds(from website: URL, completion: @escaping (Result<FeedInfo, ExtractingError>) -> Void) {
        completion(.success(feedInfo))
    }
}

class FailingFeedInfoExtractor: FeedExtracting {
    private let error: ExtractingError
    
    init(error: ExtractingError) {
        self.error = error
    }
    
    func feeds(from website: URL, completion: @escaping (Result<FeedInfo, ExtractingError>) -> Void) {
        completion(.failure(error))
    }
}

class SpyingFeedInfoExtractor: FeedExtracting {
    private(set) var website: URL?
    
    func feeds(from website: URL, completion: @escaping (Result<FeedInfo, ExtractingError>) -> Void) {
        self.website = website
    }
}

class FakeFeedInfoExtractor: FeedExtracting {
    private let stubs: [URL: FeedInfo]
    
    init(stubs: [URL: FeedInfo]) {
        self.stubs = stubs
    }
    
    func feeds(from website: URL, completion: @escaping (Result<FeedInfo, ExtractingError>) -> Void) {
        if let stub = stubs[website] {
            completion(.success(stub))
        } else {
            completion(.failure(.empty))
        }
    }
}

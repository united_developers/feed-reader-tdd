struct AppConfig {
    let fakeFeedParsing: Bool
    let removeSubscriptions: Bool
    
    init(processStages: [String]) {
        fakeFeedParsing = processStages.contains(AppProcessStages.fakeFeedParsing.rawValue)
        removeSubscriptions = processStages.contains(AppProcessStages.removeSubscriptions.rawValue)
    }
}

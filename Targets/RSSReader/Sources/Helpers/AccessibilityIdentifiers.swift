enum AccessibilityIdentifier: String {
    case feedScreen
    case subscribeButton
    case feedAddressTextField
    case subscriptionErrorLabel
    
    case subscriptionsPage
    case subscriptionTitle
    case subscriptionSubtitle
}

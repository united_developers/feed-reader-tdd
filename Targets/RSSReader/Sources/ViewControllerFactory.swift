import UIKit

final class ViewControllerFactory {
    private let config: AppConfig
    private let subscriptionsStorage: SubscriptionsStorage
    
    init(config: AppConfig) {
        self.config = config
        
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("subscriptions.json")
        
        self.subscriptionsStorage = CodableSubscriptionsStorage(path: url)
        
        if config.removeSubscriptions {
            self.subscriptionsStorage.remove()
        }
    }
    
    func makeSubscribeToFeed(delegate: FeedViewControllerDelegate) -> UIViewController {
        let viewModel = FeedViewModel(feedInfoExtractor: makeFeedExtracor(), feedInfoSaver: makeInfoSaver())
        
        let result = FeedViewController(viewModel: viewModel)
        result.delegate = delegate
        
        return result
    }
    
    func makeSubscriptions() -> UIViewController {
        let viewModel = SubscriptionsViewModel(storage: subscriptionsStorage)
        return SubscriptionsViewController(viewModel: viewModel)
    }
    
    func makeFeedExtracor() -> FeedExtracting {
        return config.fakeFeedParsing ? FakeFeedInfoExtractor(stubs: [lh: lhFeed]) : FeedExtractor()
    }
    
    private func makeInfoSaver() -> FeedInfoSaving {
        FeedInfoSaver(storage: subscriptionsStorage)
    }
}

import Foundation

enum ExtractingError: Error {
    case empty
}

protocol FeedExtracting {
    func feeds(from website: URL, completion: @escaping (Result<FeedInfo, ExtractingError>) -> Void)
}

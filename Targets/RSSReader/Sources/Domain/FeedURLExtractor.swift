import Foundation

class FeedExtractor: FeedExtracting {
    func feeds(from website: URL, completion: @escaping (Result<FeedInfo, ExtractingError>) -> Void) {
        completion(.success(lhFeed))
    }
}
